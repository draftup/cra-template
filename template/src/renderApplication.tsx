import React from "react";
import ReactDOM from "react-dom";

import { ViewComponent } from "application/view";

const rootNode = document.getElementById("root");

ReactDOM.render(<ViewComponent />, rootNode);
