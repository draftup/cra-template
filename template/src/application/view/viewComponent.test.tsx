import React from "react";
import { render } from "@testing-library/react";

import { ViewComponent } from "application/view";

test("renders view", () => {
  render(<ViewComponent />);
});
