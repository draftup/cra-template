# cra-template-draftup

This is the custom template for [Create React App](https://github.com/facebook/create-react-app) from DraftUp team. It's based on the official Typescript CRA template. 

To use this template, add `--template draftup` when creating a new app.

For example:

```sh
npx create-react-app my-app --template draftup

# or

yarn create react-app my-app --template draftup
```

For more information, please refer to:

- [Getting Started](https://create-react-app.dev/docs/getting-started) – How to create a new app.
- [User Guide](https://create-react-app.dev) – How to develop apps bootstrapped with Create React App.
